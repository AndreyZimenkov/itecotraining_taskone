package ru.iteco.training.jdbc.dao;

import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.jdbc.Connector;
import ru.iteco.training.jdbc.logic.City;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CityDAO extends AbstractDAO<City, Long> {
    public CityDAO(Connector connector) {
        super(connector);
    }

    @Override
    String getSaveSql(City dbObject) {
        return "insert into city (name) values ('"
                + dbObject.getName() + "')";
    }

    @Override
    String getUpdateSql(City dbObject) {
        return "update city set "
                + "name='" + dbObject.getName() + "'"
                + (dbObject.getMayorId() != null ? ",mayor_id='" + dbObject.getMayorId() + "'" : "")
                + (dbObject.getRegionId() != null ? ",region_id='" + dbObject.getRegionId() + "'" : "")
                + " where city_id=" + dbObject.getId();
    }

    @Override
    String getDeleteSql(City dbObject) {
        return "delete from city where city_id="
                + dbObject.getId();
    }

    @Override
    String getColumnId() {
        return "city_id";
    }

    @Override
    String getSequence() {
        return "city_city_id_seq";
    }

    @Override
    City mapping(ResultSet resultSet) throws DAORuntimeException {
        City city = new City();
        try {
            city.setId(resultSet.getLong("city_id"));
            city.setMayorId(resultSet.getLong("mayor_id"));
            city.setName(resultSet.getString("name"));
            city.setRegionId(resultSet.getLong("region_id"));
        } catch (SQLException e) {
            throw new DAORuntimeException("Ошибка при мапинге объекта: " + e.getMessage());
        }
        return city;
    }

    @Override
    public List<City> getAll() throws DAORuntimeException {
        return super.getAll(City.class);
    }

    @Override
    public City getById(Long id) throws DAORuntimeException {
        return super.getById(City.class, id);
    }

    @Override
    public Long save(City dbObject) throws DAORuntimeException {
        Long id = super.save(dbObject);
        dbObject.setId(id);
        return id;
    }

    @Override
    public void update(City dbObject) throws DAORuntimeException {
        super.update(dbObject);
    }

    @Override
    public void delete(City dbObject) throws DAORuntimeException {
        super.delete(dbObject);
    }
}
