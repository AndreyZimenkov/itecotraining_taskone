package ru.iteco.training.jdbc;

import ru.iteco.training.dai.exceptions.DAORuntimeException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private String url;
    private String driver;
    private String username;
    private String password;

    public Connector(String url, String driver, String username, String password) {
        this.url = url;
        this.driver = driver;
        this.username = username;
        this.password = password;
    }

    public Connection connect() throws DAORuntimeException {
        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException e){
            throw new DAORuntimeException("Ошибка при создание коннекта: "+e.getMessage());
        }
    }
}
