package ru.iteco.training.jdbc.logic;

public class Mayor {
    private Long id;
    private String fio;

    public Mayor() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }
}
