package ru.iteco.training.jdbc.dao;

import ru.iteco.training.dai.IDAO;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.jdbc.Connector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDAO<T, ID> implements IDAO<T, ID> {
    private Connector connector;

    public AbstractDAO(Connector connector) {
        this.connector = connector;
    }

    abstract String getSaveSql(T dbObject);
    abstract String getUpdateSql(T dbObject);
    abstract String getDeleteSql(T dbObject);
    abstract String getSequence();
    abstract T mapping(ResultSet resultSet) throws DAORuntimeException;
    abstract String getColumnId();

    @Override
    public ID save(T dbObject) throws DAORuntimeException {
        try(Connection connection = connector.connect()){
            Statement statement = connection.createStatement();
            statement.execute(getSaveSql(dbObject));
            ResultSet resultSet = statement.executeQuery(
                    "select currval('"+getSequence()+"')"
            );
            resultSet.next();
            return (ID)resultSet.getObject(1);
        } catch (SQLException e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }

    @Override
    public void update(T dbObject) throws DAORuntimeException {
        try(Connection connection = connector.connect()){
            Statement statement = connection.createStatement();
            statement.execute(getUpdateSql(dbObject));
        } catch (SQLException e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }

    @Override
    public void delete(T dbObject) throws DAORuntimeException {
        try(Connection connection = connector.connect()){
            Statement statement = connection.createStatement();
            statement.execute(getDeleteSql(dbObject));
        } catch (SQLException e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }

    @Override
    public List<T> getAll(Class<T> objectClass) throws DAORuntimeException {
        List<T> dbObjects = new ArrayList<>();
        try(Connection connection = connector.connect()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select * from "+objectClass.getSimpleName()
            );
            while(resultSet.next()){
                dbObjects.add(mapping(resultSet));
            }
            return dbObjects;
        } catch (SQLException e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }

    @Override
    public T getById(Class<T> objectClass, ID id) throws DAORuntimeException {
        try(Connection connection = connector.connect()){
            PreparedStatement statement = connection.prepareStatement(
                    "select * from "+objectClass.getSimpleName()+" where "+getColumnId()+"=?"
            );
            statement.setObject(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                return mapping(resultSet);
            }else return null;
        } catch (SQLException e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }
}
