package ru.iteco.training.jdbc.logic;

public class City {
    private Long id;
    private String name;
    private Long mayorId;
    private Long regionId;
    private Long version;

    public City() {
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMayorId() {
        return mayorId;
    }

    public void setMayorId(Long mayorId) {
        this.mayorId = mayorId;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }
}
