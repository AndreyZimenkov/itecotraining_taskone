package ru.iteco.training.dai;

import ru.iteco.training.dai.exceptions.DAORuntimeException;

import java.util.List;

/**
 * Работа с объектами бд
 * @param <T> тип объекта бд
 * @param <ID> тип ид
 */
public interface IDAO<T, ID> {

    /**
     * Добавление объекта в базу
     * @param dbObject добавляемый объект
     * @return ид объкта в базе
     */
    ID save(T dbObject) throws DAORuntimeException;

    /**
     * Обновление объекта в бд
     * @param dbObject обновляемый объект
     */
    void update(T dbObject) throws DAORuntimeException;

    /**
     * Удаление объекта из бд
     * @param dbObject удаляемый объект
     */
    void delete(T dbObject) throws DAORuntimeException;

    List<T> getAll() throws DAORuntimeException;

    /**
     * Найти в бд все объекты определённого класса
     * @param objectClass класс объектов
     * @return коллекция объектов
     */
    List<T> getAll(Class<T> objectClass) throws DAORuntimeException;

    T getById(ID id) throws DAORuntimeException;

    /**
     * Найти объект в бд по ид
     * @param objectClass класс объекта
     * @param id ид объекта в бд
     * @return объект из бд
     */
    T getById(Class<T> objectClass, ID id) throws DAORuntimeException;
}
