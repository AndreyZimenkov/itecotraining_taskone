package ru.iteco.training.dai.exceptions;

/**
 * Исключения возникающие при мапинге объектов бд и работе с ними
 */
public class DAORuntimeException extends RuntimeException {
    public DAORuntimeException(String message) {
        super(message);
    }
}
