package ru.iteco.training.hibernate.dao;

import org.hibernate.SessionFactory;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.hibernate.logic.Attribute;

import java.util.List;

public class AttributeDAO extends AbstractDAO<Attribute, Long> {

    public AttributeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Attribute> getAll() throws DAORuntimeException {
        return super.getAll(Attribute.class);
    }

    @Override
    public Long save(Attribute dbObject) throws DAORuntimeException {
        return super.save(dbObject);
    }

    @Override
    public void update(Attribute dbObject) throws DAORuntimeException {
        super.update(dbObject);
    }

    @Override
    public void delete(Attribute dbObject) throws DAORuntimeException {
        super.delete(dbObject);
    }

    @Override
    public Attribute getById(Long id) throws DAORuntimeException {
        return super.getById(Attribute.class, id);
    }
}
