package ru.iteco.training.hibernate.dao;

import org.hibernate.SessionFactory;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.hibernate.logic.AttributeType;

import java.util.List;

public class AttributeTypeDAO extends AbstractDAO<AttributeType, Long> {

    public AttributeTypeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Long save(AttributeType dbObject) throws DAORuntimeException {
        return super.save(dbObject);
    }

    @Override
    public void update(AttributeType dbObject) throws DAORuntimeException {
        super.update(dbObject);
    }

    @Override
    public void delete(AttributeType dbObject) throws DAORuntimeException {
        super.delete(dbObject);
    }

    @Override
    public List<AttributeType> getAll() throws DAORuntimeException {
        return super.getAll(AttributeType.class);
    }

    @Override
    public AttributeType getById(Long id) throws DAORuntimeException {
        return super.getById(AttributeType.class, id);
    }
}
