package ru.iteco.training.hibernate.logic;

import javax.persistence.*;

@Entity(name = "mayor")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "mayor_id")),
        @AttributeOverride(name = "name", column = @Column(name = "fio"))
})
@SequenceGenerator(name = "myGenerator", sequenceName = "mayor_mayor_id_seq",
        allocationSize = 1)
public class Mayor extends AbstractEntity {

    @OneToOne(mappedBy = "mayor", fetch = FetchType.EAGER)
    private City city;

    public Mayor() {
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
