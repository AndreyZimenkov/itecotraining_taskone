package ru.iteco.training.hibernate.dao;

import org.hibernate.SessionFactory;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.hibernate.logic.Country;

import java.util.List;

public class CountryDAO extends AbstractDAO<Country, Long> {

    public CountryDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Country> getAll() throws DAORuntimeException {
        return super.getAll(Country.class);
    }

    @Override
    public Country getById(Long id) throws DAORuntimeException {
        return super.getById(Country.class, id);
    }

    @Override
    public Long save(Country dbObject) throws DAORuntimeException {
        return super.save(dbObject);
    }

    @Override
    public void update(Country dbObject) throws DAORuntimeException {
        super.update(dbObject);
    }

    @Override
    public void delete(Country dbObject) throws DAORuntimeException {
        super.delete(dbObject);
    }
}
