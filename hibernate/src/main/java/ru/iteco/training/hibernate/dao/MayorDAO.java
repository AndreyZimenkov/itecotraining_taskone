package ru.iteco.training.hibernate.dao;

import org.hibernate.SessionFactory;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.hibernate.logic.Mayor;

import java.util.List;

public class MayorDAO extends AbstractDAO<Mayor, Long> {

    public MayorDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Mayor> getAll() throws DAORuntimeException {
        return super.getAll(Mayor.class);
    }

    @Override
    public Mayor getById(Long id) throws DAORuntimeException {
        return super.getById(Mayor.class, id);
    }

    @Override
    public Long save(Mayor dbObject) throws DAORuntimeException {
        return super.save(dbObject);
    }

    @Override
    public void update(Mayor dbObject) throws DAORuntimeException {
        super.update(dbObject);
    }

    @Override
    public void delete(Mayor dbObject) throws DAORuntimeException {
        super.delete(dbObject);
    }
}
