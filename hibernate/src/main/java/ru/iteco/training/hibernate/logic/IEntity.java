package ru.iteco.training.hibernate.logic;

public interface IEntity {
    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    Long getVersion();

    void setVersion(Long version);
}
