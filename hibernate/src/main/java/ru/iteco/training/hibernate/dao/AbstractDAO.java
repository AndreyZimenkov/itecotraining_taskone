package ru.iteco.training.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import ru.iteco.training.dai.IDAO;
import ru.iteco.training.dai.exceptions.DAORuntimeException;

import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractDAO<T, ID> implements IDAO<T, ID> {
    private SessionFactory sessionFactory;

    public AbstractDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public ID save(T dbObject) throws DAORuntimeException {
        ID id = null;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            id = (ID) session.save(dbObject);
            transaction.commit();
        } catch (Exception e) {
            transactionRollbackIfNeeded(transaction);
            throw new DAORuntimeException(e.getMessage());
        }
        return id;
    }

    @Override
    public void update(T dbObject) throws DAORuntimeException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(dbObject);
            transaction.commit();
        } catch (Exception e) {
            transactionRollbackIfNeeded(transaction);
            throw new DAORuntimeException(e.getMessage());
        }
    }

    @Override
    public void delete(T dbObject) throws DAORuntimeException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(dbObject);
            transaction.commit();
        } catch (Exception e) {
            transactionRollbackIfNeeded(transaction);
            throw new DAORuntimeException(e.getMessage());
        }
    }

    @Override
    public List<T> getAll(Class<T> objectClass) throws DAORuntimeException {
        List<T> objects = null;
        try (Session session = sessionFactory.openSession()) {
            CriteriaQuery<T> query = session.getCriteriaBuilder().createQuery(objectClass);
            query.from(objectClass);
            objects = session.createQuery(query).getResultList();
        } catch (Exception e) {
            throw new DAORuntimeException(e.getMessage());
        }
        return objects;
    }

    @Override
    public T getById(Class<T> objectClass, ID id) throws DAORuntimeException {
        T dbObject = null;
        try (Session session = sessionFactory.openSession()) {
            dbObject = session.get(objectClass, (Serializable) id);
        } catch (Exception e) {
            throw new DAORuntimeException(e.getMessage());
        }
        return dbObject;
    }

    private void transactionRollbackIfNeeded(Transaction transaction) {
        if (transaction != null && (transaction.getStatus() == TransactionStatus.ACTIVE
                || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK)) {
            transaction.rollback();
        }
    }
}
