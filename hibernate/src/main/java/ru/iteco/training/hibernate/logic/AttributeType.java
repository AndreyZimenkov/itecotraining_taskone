package ru.iteco.training.hibernate.logic;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity(name = "attribute_type")
@SequenceGenerator(name = "myGenerator",
        sequenceName = "attribute_type_attribute_type_id_seq", allocationSize = 1)
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "attribute_type_id"))
})
public class AttributeType extends AbstractEntity{

    @OneToMany(mappedBy = "attributeType", fetch = FetchType.EAGER)
    private Collection<Attribute> attributes = new HashSet<>();

    public AttributeType() {
    }

    public Collection<Attribute> getAttributes() {
        return attributes;
    }
}
