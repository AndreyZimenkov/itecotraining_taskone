package ru.iteco.training.hibernate.logic;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity(name = "city")
@SequenceGenerator(name = "myGenerator", sequenceName = "city_city_id_seq",
        allocationSize = 1)
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "city_id"))
})
public class City extends AbstractEntity{

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "mayor_id")
    private Mayor mayor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "region_id")
    private Region region;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "city_attribute",
            joinColumns = {@JoinColumn(name = "city_id")},
            inverseJoinColumns = {@JoinColumn(name = "attribute_id")}
    )
    private Collection<Attribute> attributes = new HashSet<>();

    public City() {
    }

    public Mayor getMayor() {
        return mayor;
    }

    public void setMayor(Mayor mayor) {
        this.mayor = mayor;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Collection<Attribute> getAttributes() {
        return attributes;
    }
}
