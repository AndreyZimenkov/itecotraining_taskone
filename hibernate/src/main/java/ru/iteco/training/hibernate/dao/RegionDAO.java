package ru.iteco.training.hibernate.dao;

import org.hibernate.SessionFactory;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.hibernate.logic.Region;

import java.util.List;

public class RegionDAO extends AbstractDAO<Region, Long> {

    public RegionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Region> getAll() throws DAORuntimeException {
        return super.getAll(Region.class);
    }

    @Override
    public Region getById(Long id) throws DAORuntimeException {
        return super.getById(Region.class, id);
    }

    @Override
    public Long save(Region dbObject) throws DAORuntimeException {
        return super.save(dbObject);
    }

    @Override
    public void update(Region dbObject) throws DAORuntimeException {
        super.update(dbObject);
    }

    @Override
    public void delete(Region dbObject) throws DAORuntimeException {
        super.delete(dbObject);
    }
}
