package ru.iteco.training.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.iteco.training.hibernate.logic.*;

public class Configurator {
    private SessionFactory sessionFactory;
    private String confFilePath;

    public Configurator(String confFilePath) {
        this.confFilePath = confFilePath;
        try {
            Configuration configuration = new Configuration()
                    .configure(confFilePath)
                    .addAnnotatedClass(City.class)
                    .addAnnotatedClass(Attribute.class)
                    .addAnnotatedClass(AttributeType.class)
                    .addAnnotatedClass(Country.class)
                    .addAnnotatedClass(Region.class)
                    .addAnnotatedClass(Mayor.class);
            sessionFactory = configuration.buildSessionFactory();
        } catch (Exception ex) {
            System.out.println("Initial SessionFactory creation failed.\n" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public String getConfFilePath() {
        return confFilePath;
    }
}
