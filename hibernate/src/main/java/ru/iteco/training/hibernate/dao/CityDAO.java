package ru.iteco.training.hibernate.dao;

import org.hibernate.SessionFactory;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.hibernate.logic.City;

import java.util.List;

public class CityDAO extends AbstractDAO<City, Long> {

    public CityDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<City> getAll() throws DAORuntimeException {
        return super.getAll(City.class);
    }

    @Override
    public City getById(Long id) throws DAORuntimeException{
        return super.getById(City.class, id);
    }

    @Override
    public Long save(City dbObject) throws DAORuntimeException{
        return super.save(dbObject);
    }

    @Override
    public void update(City dbObject) throws DAORuntimeException{
        super.update(dbObject);
    }

    @Override
    public void delete(City dbObject) throws DAORuntimeException{
        super.delete(dbObject);
    }
}
