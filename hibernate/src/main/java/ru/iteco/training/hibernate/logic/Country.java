package ru.iteco.training.hibernate.logic;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity(name = "country")
@SequenceGenerator(name = "myGenerator", sequenceName = "country_country_id_seq",
        allocationSize = 1)
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "country_id"))
})
public class Country extends AbstractEntity {

    @OneToMany(mappedBy = "country", fetch = FetchType.EAGER)
    private Collection<Region> regions = new HashSet<>();

    public Country() {
    }

    public Collection<Region> getRegions() {
        return regions;
    }
}
