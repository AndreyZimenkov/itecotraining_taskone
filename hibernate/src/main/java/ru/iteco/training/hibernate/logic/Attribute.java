package ru.iteco.training.hibernate.logic;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity(name = "attribute")
@SequenceGenerator(name = "myGenerator", sequenceName = "attribute_attribute_id_seq",
        allocationSize = 1)
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "attribute_id"))
})
public class Attribute extends AbstractEntity{

    @Column(name = "value")
    private String value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "attribute_type_id")
    private AttributeType attributeType;

    @ManyToMany(mappedBy = "attributes", fetch = FetchType.EAGER)
    private Collection<City> cities = new HashSet<>();

    public Attribute() {
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Collection<City> getCities() {
        return cities;
    }
}
