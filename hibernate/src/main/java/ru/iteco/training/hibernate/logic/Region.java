package ru.iteco.training.hibernate.logic;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity(name = "region")
@SequenceGenerator(name = "myGenerator", sequenceName = "region_region_id_seq",
        allocationSize = 1)
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "region_id"))
})
public class Region extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(mappedBy = "region", fetch = FetchType.EAGER)
    private Collection<City> cities = new HashSet<>();

    public Region() {
    }

    public Collection<City> getCities() {
        return cities;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
