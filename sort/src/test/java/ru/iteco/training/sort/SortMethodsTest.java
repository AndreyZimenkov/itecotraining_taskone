package ru.iteco.training.sort;

import org.junit.Test;
import ru.iteco.training.sort.pivotStrategy.PivotStrategyFirst;
import ru.iteco.training.sort.pivotStrategy.PivotStrategyLast;
import ru.iteco.training.sort.pivotStrategy.PivotStrategyMiddle;
import ru.iteco.training.sort.sortStrategy.BubbleSort;
import ru.iteco.training.sort.sortStrategy.QuickSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SortMethodsTest {

    String[] unsortedArray = {"2", "3", "1", "1", "3", "4", "8", "9", "2"};
    String[] sortedArray = {"1", "1", "2", "2", "3", "3", "4", "8", "9"};

    @Test
    public void testBubbleSortNormalSort() {
        List<String> list = new ArrayList<String>(Arrays.asList(unsortedArray));
        BubbleSort sorter = new BubbleSort();
        sorter.sort(list, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);

                        if (x == y) return 0;
                        return x > y ? 1 : -1;
                    }
                });
        assertEquals(Arrays.asList(sortedArray), list);
    }

    @Test
    public void testQuickSortFirstPivotPoint(){
        List<String> list = new ArrayList<String>(Arrays.asList(unsortedArray));
        QuickSort sorter = new QuickSort(new PivotStrategyFirst());
        sorter.sort(list, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);

                        if (x == y) return 0;
                        return x > y ? 1 : -1;
                    }
                });
        assertEquals(Arrays.asList(sortedArray), list);
    }

    @Test
    public void testQuickSortMiddlePivotPoint(){
        List<String> list = new ArrayList<String>(Arrays.asList(unsortedArray));
        QuickSort sorter = new QuickSort(new PivotStrategyMiddle());
        sorter.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                int x = Integer.parseInt(o1);
                int y = Integer.parseInt(o2);

                if (x == y) return 0;
                return x > y ? 1 : -1;
            }
        });
        assertEquals(Arrays.asList(sortedArray), list);
    }

    @Test
    public void testQuickSortLastPivotPoint(){
        List<String> list = new ArrayList<String>(Arrays.asList(unsortedArray));
        QuickSort sorter = new QuickSort(new PivotStrategyLast());
        sorter.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                int x = Integer.parseInt(o1);
                int y = Integer.parseInt(o2);

                if (x == y) return 0;
                return x > y ? 1 : -1;
            }
        });
        assertEquals(Arrays.asList(sortedArray), list);
    }
}
