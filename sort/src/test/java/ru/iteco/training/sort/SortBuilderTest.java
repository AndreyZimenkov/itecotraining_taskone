package ru.iteco.training.sort;

import org.junit.Test;
import ru.iteco.training.sort.pivotStrategy.PivotStrategyType;
import ru.iteco.training.sort.sortStrategy.SortMethodType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

public class SortBuilderTest {

    String[] unsortedArray = {"2", "3", "1"};

    @Test
    public void testNewBuilder() {
        SortBuilder builder = SortBuilder.newBuilder();
        assertNotNull(builder);
    }

    @Test
    public void testReturnOnlyView() {
        SortBuilder builder = SortBuilder.newBuilder().returnOnlyView(true);
        assertNotNull(builder);
    }

    @Test
    public void testSortMethod() {
        SortBuilder builder = SortBuilder.newBuilder().sortMethod(SortMethodType.DEFAULT);
        assertNotNull(builder);
    }

    @Test
    public void testPivotStrategy() {
        SortBuilder builder = SortBuilder.newBuilder().pivotStrategy(PivotStrategyType.DEFAULT);
        assertNotNull(builder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildNoParameters() {
        Sorter<String> sorter = SortBuilder.newBuilder()
                .build(BuildType.MUTABLE, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        return 0;
                    }
                });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildNoPivotPoint() {
        Sorter<String> sorter = SortBuilder.newBuilder()
                .sortMethod(SortMethodType.QUICK)
                .build(BuildType.MUTABLE, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        return 0;
                    }
                });
    }

    @Test
    public void testSortEmptyArray() {
        List<String> list = new ArrayList<String>();
        Sorter<String> sorter = SortBuilder.newBuilder()
                .sortMethod(SortMethodType.DEFAULT)
                .pivotStrategy(PivotStrategyType.DEFAULT)
                .returnOnlyView(false)
                .build(BuildType.MUTABLE, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);

                        if (x == y) return 0;
                        return x > y ? 1 : -1;
                    }
                });
        assertEquals(sorter.sort(list), list);
    }

    @Test
    public void testSortImmutable() {
        List<String> list = new ArrayList<String>(Arrays.asList(unsortedArray));
        Sorter<String> sorter = SortBuilder.newBuilder()
                .sortMethod(SortMethodType.DEFAULT)
                .pivotStrategy(PivotStrategyType.DEFAULT)
                .returnOnlyView(false)
                .build(BuildType.IMMUTABLE, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);

                        if (x == y) return 0;
                        return x > y ? 1 : -1;
                    }
                });
        assertTrue(list != sorter.sort(list));
    }

    @Test
    public void testSortMutable() {
        List<String> list = new ArrayList<String>(Arrays.asList(unsortedArray));
        Sorter<String> sorter = SortBuilder.newBuilder()
                .sortMethod(SortMethodType.DEFAULT)
                .pivotStrategy(PivotStrategyType.DEFAULT)
                .returnOnlyView(false)
                .build(BuildType.MUTABLE, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);

                        if (x == y) return 0;
                        return x > y ? 1 : -1;
                    }
                });
        assertTrue(list == sorter.sort(list));
    }
}
