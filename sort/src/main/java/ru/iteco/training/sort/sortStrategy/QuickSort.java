package ru.iteco.training.sort.sortStrategy;

import ru.iteco.training.sort.pivotStrategy.PivotStrategy;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class QuickSort implements SortMethod {
    private PivotStrategy pivotStrategy;

    public QuickSort(PivotStrategy pivot) {
        pivotStrategy = pivot;
    }

    /**
     * быстрая сортировка
     */
    public <T> void sort(List<T> list, Comparator<T> comparator) {
        doSort(list, comparator, 0, list.size()-1);
    }

    private <T> void doSort(List<T> list, Comparator<T> comparator, int start, int end) {
        if (start >= end) {
            return;
        }

        T tempObj;
        int i = start, j = end;
        int pvPoint = pivotStrategy.get(i, j);
        while (i <= j) {
            while (comparator.compare(list.get(i), list.get(pvPoint)) == -1) {
                i++;
            }
            while (comparator.compare(list.get(j), list.get(pvPoint)) == 1) {
                j--;
            }
            if (i <= j) {
                tempObj = list.get(i);
                list.set(i, list.get(j));
                list.set(j, tempObj);
                i++;
                j--;
            }
        }
        if(start < j){
            doSort(list, comparator, start, j);
        }
        if(end>i){
            doSort(list, comparator, i, end);
        }
    }
}