package ru.iteco.training.sort;

public enum BuildType {
    MUTABLE, IMMUTABLE, DEFAULT, NOTSELECTED
}
