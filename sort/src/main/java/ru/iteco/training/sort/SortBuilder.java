package ru.iteco.training.sort;

import ru.iteco.training.sort.pivotStrategy.*;
import ru.iteco.training.sort.sortStrategy.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortBuilder {
    private SortMethodType sortMethodType;
    private PivotStrategyType pivotStrategyType;
    private BuildType buildType;
    private boolean returnViewOnly = false;

    private SortBuilder() {
    }

    static public SortBuilder newBuilder() {
        return new SortBuilder();
    }

    public SortBuilder pivotStrategy(PivotStrategyType type) {
        pivotStrategyType = type;
        return this;
    }

    public SortBuilder sortMethod(SortMethodType type) {
        sortMethodType = type;
        return this;
    }

    public SortBuilder returnOnlyView(boolean view) {
        returnViewOnly = view;
        return this;
    }

    public <T> Sorter<T> build(BuildType type, Comparator<T> comparator) {
        buildType = type;
        return returnViewOnly ? new BaseSorter<T>(null, comparator) {
            @Override
            public List<T> sort(List<T> list) {
                return Collections.unmodifiableList(list);
            }
        } : buildByType(buildSortMethod(), comparator);
    }

    private SortMethod buildSortMethod() {
        if (sortMethodType == null) sortMethodType = SortMethodType.NOTSELECTED;
        switch (sortMethodType) {
            case QUICK:
                return new QuickSort(buildPivotStrategy());
            case BUBBLE:
                return new BubbleSort();
            case DEFAULT:
                return new BubbleSort();
            default:
                throw new IllegalArgumentException("Не выбран алгоритм сортировки");
        }
    }

    private PivotStrategy buildPivotStrategy() {
        if (pivotStrategyType == null) pivotStrategyType = PivotStrategyType.NOTSELECTED;
        switch (pivotStrategyType) {
            case FIRST:
                return new PivotStrategyFirst();
            case MIDDLE:
                return new PivotStrategyMiddle();
            case LAST:
                return new PivotStrategyLast();
            case DEFAULT:
                return new PivotStrategyFirst();
            default:
                throw new IllegalArgumentException("Не выбрана тока привязки");
        }
    }

    private <T> Sorter<T> buildByType(SortMethod sortMethod, Comparator<T> comparator) {
        if (buildType == null) buildType = BuildType.NOTSELECTED;
        switch (buildType) {
            case DEFAULT:
                return new MutableSort<T>(sortMethod, comparator);
            case MUTABLE:
                return new MutableSort<T>(sortMethod, comparator);
            case IMMUTABLE:
                return new ImmutableSort<T>(sortMethod, comparator);
            default:
                throw new IllegalArgumentException("Невыбран способ сборки сортировщика");
        }
    }
}
