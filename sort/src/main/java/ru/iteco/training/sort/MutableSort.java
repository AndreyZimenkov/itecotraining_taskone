package ru.iteco.training.sort;

import ru.iteco.training.sort.sortStrategy.SortMethod;

import java.util.Comparator;
import java.util.List;

public class MutableSort<T> extends BaseSorter<T> {
    MutableSort(SortMethod sortMethod, Comparator<T> comparator) {
        super(sortMethod, comparator);
    }

    /**
     * @return ссылка на входную коллекцию
     */
    @Override
    public List<T> sort(List<T> list) {
        return super.sort(list);
    }
}
