package ru.iteco.training.sort;

import ru.iteco.training.sort.sortStrategy.SortMethod;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ImmutableSort<T> extends BaseSorter<T> {

    ImmutableSort(SortMethod sortMethod, Comparator<T> comparator) {
        super(sortMethod, comparator);
    }

    /**
     * @return копия входной коллекции
     */
    @Override
    public List<T> sort(List<T> list) {
        List<T> newList = new ArrayList<T>(list);
        return super.sort(newList);
    }
}
