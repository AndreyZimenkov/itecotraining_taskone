package ru.iteco.training.sort;

import java.util.Comparator;
import java.util.List;

/**
 * сортировка объектов
 * @param <T> тип сортируемых объектов
 */

public interface Sorter<T> {

    /**
     * сортировка пользовательской коллекции
     * @param list пользовательская коллекция
     * @return отсортированная коллекция
     */

    List<T> sort(List<T> list);
}
