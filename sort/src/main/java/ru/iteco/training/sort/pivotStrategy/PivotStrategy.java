package ru.iteco.training.sort.pivotStrategy;

import java.util.List;

/**
 * опорная точка для алгоритма быстрой сортировки
 */

public interface PivotStrategy {
    int get(int start, int end);
}
