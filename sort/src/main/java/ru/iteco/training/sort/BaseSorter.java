package ru.iteco.training.sort;

import ru.iteco.training.sort.sortStrategy.SortMethod;

import java.util.Comparator;
import java.util.List;

public class BaseSorter<T> implements Sorter<T> {
    private SortMethod sortMethod;
    private Comparator<T> comparator;

    BaseSorter(SortMethod sortMethod, Comparator<T> comparator) {
        this.sortMethod = sortMethod;
        this.comparator = comparator;
    }

    public List<T> sort(List<T> list) {
        sortMethod.sort(list, comparator);
        return list;
    }
}
