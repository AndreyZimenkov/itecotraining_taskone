package ru.iteco.training.sort.sortStrategy;

public enum SortMethodType {
    QUICK, BUBBLE, DEFAULT, NOTSELECTED
}
