package ru.iteco.training.sort.sortStrategy;

import java.util.Comparator;
import java.util.List;

/**
 * алгоритм сортировки
 */

public interface SortMethod {
    /**
     * сортировка пользовательской коллекции по выбраному алгоритму
     * @param list пользовательская коллекция
     * @param comparator компаратор для сравнения знаений в коллекции
     * @param <T> тип объектов в коллекции
     * @return отсортированная коллекция
     */
    <T> void sort(List<T> list, Comparator<T> comparator);
}
