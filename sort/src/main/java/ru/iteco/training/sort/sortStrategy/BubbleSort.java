package ru.iteco.training.sort.sortStrategy;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BubbleSort implements SortMethod {
    /**
     * сортировка пузырьком
     */
    public <T> void sort(List<T> list, Comparator<T> comparator) {
        int i, j;
        T tempObj;

        for (i = 0; i < list.size(); i++) {
            for (j = list.size() - 1; j > i; j--) {
                if (comparator.compare(list.get(j - 1), list.get(j)) == 1) {
                    tempObj = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, tempObj);
                }
            }
        }
    }
}
