package ru.iteco.training.sort.pivotStrategy;

import java.util.List;

public class PivotStrategyMiddle implements PivotStrategy {
    public int get(int start, int end) {
        return start + (end - start) / 2;
    }
}
