package ru.iteco.training.sort.pivotStrategy;

public enum PivotStrategyType {
    FIRST, MIDDLE, LAST, DEFAULT, NOTSELECTED
}
