package ru.iteco.training;

import org.junit.Test;
import ru.iteco.training.dai.exceptions.DAORuntimeException;
import ru.iteco.training.jdbc.Connector;
import ru.iteco.training.jdbc.dao.CityDAO;
import ru.iteco.training.jdbc.logic.City;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class JDBCTest {

    @Test
    public void cityDAOTest(){
        String url = "jdbc:postgresql://localhost:5432/test",
                username = "postgres",
                password = "123gg",
                driver = "org.postgresql.Driver";
        Connector connector = new Connector(url, driver, username, password);
        City city = new City();
        CityDAO cityDAO = new CityDAO(connector);
        Long id = null;

        city.setName("jdbc city");
        try {
            id = cityDAO.save(city);
            assertEquals(cityDAO.getById(id).getName(), city.getName());

            city.setName("jdbc update city");
            cityDAO.update(city);
            assertEquals(cityDAO.getById(id).getName(), city.getName());

            cityDAO.delete(city);
            assertNull(cityDAO.getById(id));
        } catch (DAORuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}
