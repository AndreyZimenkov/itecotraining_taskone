package ru.iteco.training;

import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.dialect.lock.OptimisticEntityLockException;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.junit.Test;
import ru.iteco.training.hibernate.Configurator;
import ru.iteco.training.hibernate.dao.AttributeDAO;
import ru.iteco.training.hibernate.dao.AttributeTypeDAO;
import ru.iteco.training.hibernate.dao.CityDAO;
import ru.iteco.training.hibernate.logic.Attribute;
import ru.iteco.training.hibernate.logic.AttributeType;
import ru.iteco.training.hibernate.logic.City;

import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class HibernateTest {

    private String hibernateConfFilePath = "test_hibernate.cfg.xml";

    @Test
    public void saveCityTest() {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        CityDAO cityDAO = new CityDAO(configurator.getSessionFactory());
        City city = new City();
        city.setName("hibernate city");
        Long id = cityDAO.save(city);
        assertEquals(city.getName(), cityDAO.getById(id).getName());
    }

    @Test
    public void saveAttributeTest() {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        AttributeDAO attributeDAO = new AttributeDAO(configurator.getSessionFactory());
        Attribute attribute = new Attribute();
        attribute.setName("hibernate attribute");
        Long id = attributeDAO.save(attribute);
        assertEquals(attribute.getName(), attributeDAO.getById(id).getName());
    }

    @Test
    public void saveAttributeTypeTest() {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        AttributeTypeDAO attributeTypeDAO = new AttributeTypeDAO(configurator.getSessionFactory());
        AttributeType attributeType = new AttributeType();
        attributeType.setName("hibernate attributeType");
        Long id = attributeTypeDAO.save(attributeType);
        assertEquals(attributeType.getName(), attributeTypeDAO.getById(id).getName());
    }

    @Test
    public void cityAttributeLinkTest() {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        CityDAO cityDAO = new CityDAO(configurator.getSessionFactory());
        AttributeDAO attributeDAO = new AttributeDAO(configurator.getSessionFactory());
        City city = new City();
        city.setName("link city");
        Attribute attribute = new Attribute();
        attribute.setName("link attribute");
        city.getAttributes().add(attribute);
        Long idAttr = attributeDAO.save(attribute);
        Long idCity = cityDAO.save(city);
        assertFalse(attributeDAO.getById(idAttr).getCities().contains(cityDAO.getById(idCity)));
    }

    @Test
    public void attributeAttributeTypeLinkTest() {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        AttributeTypeDAO attributeTypeDAO = new AttributeTypeDAO(configurator.getSessionFactory());
        AttributeDAO attributeDAO = new AttributeDAO(configurator.getSessionFactory());
        Attribute attribute = new Attribute();
        attribute.setName("hibernate link attribute");
        AttributeType attributeType = new AttributeType();
        attributeType.setName("hibernate link attribute type ");
        attribute.setAttributeType(attributeType);
        Long idAttrType = attributeTypeDAO.save(attributeType);
        Long idAttr = attributeDAO.save(attribute);
        assertEquals(
                attributeTypeDAO.getById(idAttrType).getName(),
                attributeDAO.getById(idAttr).getAttributeType().getName());
    }

    @Test(expected = OptimisticEntityLockException.class)
    public void cityOptimisticLockTest() {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        CityDAO cityDAO = new CityDAO(configurator.getSessionFactory());
        City city = new City();
        city.setName("opt lock test city");
        Long cityId = cityDAO.save(city);

        Configurator configurator1 = new Configurator(hibernateConfFilePath);
        Transaction transaction = null;
        try (Session session = configurator1.getSessionFactory().openSession()) {
            city.setName("opt lock test change city");
            session.buildLockRequest(new LockOptions(LockMode.OPTIMISTIC)).lock(city);
            transaction = session.beginTransaction();
            session.update(city);
            session.flush();
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                City loadCity = cityDAO.getById(cityId);
                loadCity.setName("opt lock test load city");
                cityDAO.update(loadCity);
            });
            future.join();
            assertEquals("opt lock test load city", cityDAO.getById(cityId).getName());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null
                    && (transaction.getStatus() == TransactionStatus.ACTIVE)
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw e;
        }
    }

    @Test
    public void cityPessimisticLocTest() throws InterruptedException {
        Configurator configurator = new Configurator(hibernateConfFilePath);
        CityDAO cityDAO = new CityDAO(configurator.getSessionFactory());
        City city = new City();
        city.setName("pess lock test city");
        Long cityId = cityDAO.save(city);

        Configurator configurator1 = new Configurator(hibernateConfFilePath);
        Transaction transaction = null;
        try (Session session = configurator.getSessionFactory().openSession()) {
            city.setName("pess lock test change city");
            session.buildLockRequest(new LockOptions(LockMode.PESSIMISTIC_READ)).lock(city);
            transaction = session.beginTransaction();
            session.update(city);
            session.flush();
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                City loadCity = cityDAO.getById(cityId);
                loadCity.setName("pess lock test load city");
                cityDAO.update(loadCity);
            });
            Thread.sleep(3000);
            assertEquals("pess lock test city", cityDAO.getById(cityId).getName());
            assertFalse(future.isDone());
            transaction.commit();
            future.join();
            assertEquals("pess lock test load city", cityDAO.getById(cityId).getName());
            assertTrue(future.isDone());
        } catch (Exception e) {
            if (transaction != null
                    && (transaction.getStatus() == TransactionStatus.ACTIVE)
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw e;
        }
    }
}
